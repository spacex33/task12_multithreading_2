package blocking_queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        BlockingQueue queue = new ArrayBlockingQueue(1024);

        BlockingQueueSender blockingQueueSender = new BlockingQueueSender(queue, "Hello World!");
        BlockingQueueReceiver blockingQueueReceiver = new BlockingQueueReceiver(queue);

        ExecutorService executorService = Executors.newCachedThreadPool();

        try {
            executorService.execute(blockingQueueReceiver);
            executorService.execute(blockingQueueSender);
        } finally {
            executorService.shutdown();
        }

    }
}
