package blocking_queue;

import java.util.concurrent.BlockingQueue;

public class BlockingQueueReceiver implements Runnable {

    private BlockingQueue queue = null;

    public BlockingQueueReceiver(BlockingQueue queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.print((char) queue.take());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
