package blocking_queue;

import java.util.concurrent.BlockingQueue;

public class BlockingQueueSender implements Runnable {

    private BlockingQueue queue;
    private String message;

    public BlockingQueueSender(BlockingQueue queue, String message) {
        this.queue = queue;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            for (char character : message.toCharArray()) {
                queue.put(character);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
