package custom_lock;

public class Main {

    public static void main(String[] args) {
        CustomLock customLock = new CustomLock();

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {

                customLock.lock();
                method();
                customLock.unlock();

            }).start();
        }
    }

    private static void method() {
        System.out.println(Thread.currentThread());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread());
    }
}
