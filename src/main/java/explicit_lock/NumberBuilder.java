package explicit_lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class NumberBuilder {

    private int number;
    private Lock lock = new ReentrantLock();

    public NumberBuilder(int number) {
        this.number = number;
        System.out.println(this + " initial");
    }

    public void add(int number) {
        lock.lock();
        try {
            System.out.println(this
                    + " "
                    + number
                    + " added, result: "
                    + (this.number += number)
                    + " by #"
                    + Thread.currentThread().getId());
        } finally {
            lock.unlock();
        }
    }

    public void subtract(int number) {
        lock.lock();
        try {
            System.out.println(this
                    + " "
                    + number
                    + " subtracted, result: "
                    + (this.number -= number)
                    + " by #"
                    + Thread.currentThread().getId());
        } finally {
            lock.unlock();
        }
    }

    public void multiply(int number) {
        lock.lock();
        try {
            System.out.println(this
                    + " "
                    + number
                    + " multiplied, result: "
                    + (this.number *= number)
                    + " by #"
                    + Thread.currentThread().getId());
        } finally {
            lock.unlock();
        }
    }

    @Override
    public String toString() {
        return "NumberBuilder{" +
                "number=" + number +
                '}';
    }
}
