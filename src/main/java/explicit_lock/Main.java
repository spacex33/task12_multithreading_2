package explicit_lock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        NumberBuilder numberBuilder = new NumberBuilder(2);
        try {
            for (int i = 0; i < 10; i++) {
                executorService.execute(() -> numberBuilder.add(2));
                executorService.execute(() -> numberBuilder.multiply(2));
                executorService.execute(() -> numberBuilder.subtract(6));
            }

        } finally {
            executorService.shutdown();
        }
    }
}
